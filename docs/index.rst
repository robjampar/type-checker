.. _index:

.. highlight:: python

.. toctree::
    :hidden:

    Introduction <self>

Type Checker: Python Type checking library
==========================================

**Type Checker** is a library for type checking in Python.

It can do runtime type checking on Python values.

Getting Started
---------------

Install Type Checker::

    pip install type_checker


.. code-block:: python
    :caption: simple_example.py
    :name: simple_example-py

    from type_checker import type_check

    print(type_check("hello world", str))

.. code-block::

    python simple_example.py

    >>> True


In **simple_example.py** above, Type Checker is used to type check that ``"hello world"`` is as a ``str`` type.

Supported Types
---------------

Type Checker supports checking the following values for defs:

- float
- int
- bool
- str
- tuple
- list
- dictionary
- set
- Any
- Callable
- Type


Multiple type defs can be combined using ``Union``::

    from type_checker import type_check
    from typing import Union

    type_check(10, Union[str, int])  # returns True
    type_check("hey", Union[str, int])  # returns True


Type defs inside dictionaries can be made optional::

    from type_checker import type_check
    from typing import Optional

    type_def = {
        "value": int,
        Optional["optional_key"]: str
    }

    model = {
        "value": 10
    }

    type_check(model, type_def)  # returns True

Examples:

+-------------------+--------------------+---------------------+
| Type Def          | Value              | ``type_check``      |
+===================+====================+=====================+
| str               | "hey"              | True                |
+-------------------+--------------------+---------------------+
|                   | 1                  | False               |
+-------------------+--------------------+---------------------+
| int               | 1                  | True                |
+-------------------+--------------------+---------------------+
|                   | True               | False               |
+-------------------+--------------------+---------------------+
|                   | 1.5                | False               |
+-------------------+--------------------+---------------------+
| bool              | "True"             | False               |
+-------------------+--------------------+---------------------+
|                   | True               | True                |
+-------------------+--------------------+---------------------+
| list              | []                 | True                |
+-------------------+--------------------+---------------------+
|                   | "not_a_list"       | False               |
+-------------------+--------------------+---------------------+
| []                | []                 | True                |
+-------------------+--------------------+---------------------+
|                   | 20.4               | False               |
+-------------------+--------------------+---------------------+
| [str]             | []                 | True                |
+-------------------+--------------------+---------------------+
|                   | ["hey"]            | True                |
+-------------------+--------------------+---------------------+
|                   | [1]                | False               |
+-------------------+--------------------+---------------------+
| [Union[str, int]] | []                 | True                |
+-------------------+--------------------+---------------------+
|                   | ["hey"]            | True                |
+-------------------+--------------------+---------------------+
|                   | [1]                | True                |
+-------------------+--------------------+---------------------+
|                   | ["hey", 1]         | True                |
+-------------------+--------------------+---------------------+
|                   | ["hey", 1.5]       | False               |
+-------------------+--------------------+---------------------+
| Any               | "hey"              | True                |
+-------------------+--------------------+---------------------+
|                   | 1                  | True                |
+-------------------+--------------------+---------------------+
|                   | False              | True                |
+-------------------+--------------------+---------------------+
